package sweep;

import java.util.Collections;
import java.util.List;

import common.ClosestPointFinder;
import common.ClosestPointUtils;
import common.MinimalDistancePoints;
import common.Point;

public class SweepPlaneStrategy implements ClosestPointFinder {
	ClosestPointUtils utils;

	public SweepPlaneStrategy(ClosestPointUtils utils) {
		this.utils = utils;
	}

	public MinimalDistancePoints findClosestPoint(List<Point> points) throws IllegalArgumentException {
		Collections.sort(points, new Point.XComparator());
		return findClosestPairofPoints(points);
	}

	private MinimalDistancePoints findClosestPairofPoints(List<Point> points) throws IllegalArgumentException {
		int size = points.size();

		int completedIndex = 0;
		MinimalDistancePoints minPoints = utils.bruteForce(points.subList(0, 2));
		for (int i = 3; i < size; i++) {

			Double currentX = points.get(i).getCoordinates()[0];
			for (int j = completedIndex; j < i; j++) {
				if (currentX - points.get(j).getCoordinates()[0] > minPoints.getMin()) {
					completedIndex = j;
				} else {
					break;
				}
			}
			minPoints = utils.stripClosest(points.subList(completedIndex, i + 1), minPoints);

		}

		return minPoints;
	}
}
