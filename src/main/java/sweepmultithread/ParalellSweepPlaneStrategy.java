package sweepmultithread;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import sweep.SweepPlaneStrategy;

import common.ClosestPointFinder;
import common.ClosestPointUtils;
import common.MinimalDistancePoints;
import common.Point;

public class ParalellSweepPlaneStrategy implements ClosestPointFinder {
	private ClosestPointUtils utils;
	private int threadNumber;

	public ParalellSweepPlaneStrategy(ClosestPointUtils utils, int threadNumber) {
		this.utils = utils;
		this.threadNumber = threadNumber;
	}

	public MinimalDistancePoints findClosestPoint(List<Point> points) throws IllegalArgumentException {
		Collections.sort(points, new Point.XComparator());
		int size = points.size();

		threadNumber = decreaseThreadNumber(threadNumber, size);

		if (threadNumber == 1) {
			return new SweepPlaneStrategy(utils).findClosestPoint(points);
		}

		List<Point> seperatorPoints = new ArrayList<Point>();
		List<List<Point>> subProblemSpaces = new ArrayList<List<Point>>();
		int subProblemSize = (size / threadNumber) - 1;

		List<Future<MinimalDistancePoints>> result = new ArrayList<Future<MinimalDistancePoints>>();

		subProblemSpaces.add(points.subList(0, subProblemSize));
		ExecutorService executor = Executors.newFixedThreadPool(threadNumber);
		submitCalls(result, executor, points.subList(0, subProblemSize));

		for (int i = 1; i < threadNumber; i++) {
			List<Point> subProblemSpace = null;
			seperatorPoints.add(points.get((i) * (subProblemSize + 1) - 1));
			if (i == threadNumber - 1) {
				subProblemSpace = points.subList(i * (subProblemSize + 1), points.size());
			} else {
				subProblemSpace = points.subList(i * (subProblemSize + 1), (i + 1) * (subProblemSize + 1) - 1);
			}

			subProblemSpaces.add(subProblemSpace);
			submitCalls(result, executor, subProblemSpace);
		}

		MinimalDistancePoints min = null;
		;
		try {
			min = result.get(0).get();

			for (int i = 1; i < result.size(); i++) {
				MinimalDistancePoints res = result.get(i).get();
				min = utils.getMinimalResult(min, res);
			}
		} catch (InterruptedException e) {
			System.out.println("Thread is interrupted");
			e.printStackTrace();
		} catch (ExecutionException e) {
			System.out.println("ExecutionException");
			e.printStackTrace();
		}

		for (int i = 0; i < seperatorPoints.size(); i++) {
			List<Point> pointsLeft = checkFromLeft(subProblemSpaces.get(i), seperatorPoints.get(i), min.getMin());
			List<Point> pointsRight = checkFromRight(subProblemSpaces.get(i + 1), seperatorPoints.get(i), min.getMin());
			MinimalDistancePoints resList = utils.findMinimalDistanceBetweenTwoList(pointsLeft, pointsRight, min);
			min = utils.getMinimalResult(min, resList);
			MinimalDistancePoints resLeft = utils.minimalDistanceBetweenPointAndList(pointsLeft, seperatorPoints.get(i), min);
			min = utils.getMinimalResult(min, resLeft);
			MinimalDistancePoints resRight = utils.minimalDistanceBetweenPointAndList(pointsRight, seperatorPoints.get(i), min);
			min = utils.getMinimalResult(min, resRight);

		}

		return min;

	}

	private void submitCalls(List<Future<MinimalDistancePoints>> result, ExecutorService executor, List<Point> subProblemSpace) {
		Callable<MinimalDistancePoints> callable = new SweepAlgorithmWorker(utils, subProblemSpace);
		Future<MinimalDistancePoints> future = executor.submit(callable);
		result.add(future);
	}

	private int decreaseThreadNumber(int threadNumber, int size) {
		while (size / threadNumber < 4 && threadNumber > 1) {
			threadNumber--;
		}
		return threadNumber;
	}

	public List<Point> checkFromLeft(List<Point> points, Point point, Double min) {
		for (int i = points.size() - 1; i >= 0; i--) {
			if (point.getCoordinates()[0] - points.get(i).getCoordinates()[0] > min) {
				if (i == points.size() - 1) {
					return new ArrayList<Point>();
				}
				return points.subList(i + 1, points.size());
			}

		}
		return points;

	}

	public List<Point> checkFromRight(List<Point> points, Point point, Double min) {
		for (int i = 0; i < points.size(); i++) {
			if (points.get(i).getCoordinates()[0] - point.getCoordinates()[0] > min) {
				if (i == 0) {
					return new ArrayList<Point>();
				}
				return points.subList(0, i);
			}
		}
		return points;

	}

}
