package sweepmultithread;

import java.util.List;
import java.util.concurrent.Callable;

import common.ClosestPointUtils;
import common.MinimalDistancePoints;
import common.Point;

public class SweepAlgorithmWorker implements Callable<MinimalDistancePoints> {
	ClosestPointUtils utils;
	List<Point> points;

	public SweepAlgorithmWorker(ClosestPointUtils utils, List<Point> points) {
		this.utils = utils;
		this.points = points;
	}

	private MinimalDistancePoints findClosestPoint() throws IllegalArgumentException {
		int size = points.size();

		int completedIndex = 0;
		MinimalDistancePoints minPoints = utils.bruteForce(points.subList(0, 2));
		for (int i = 3; i < size; i++) {

			Double currentX = points.get(i).getCoordinates()[0];
			for (int j = completedIndex; j < i; j++) {
				if (currentX - points.get(j).getCoordinates()[0] > minPoints.getMin()) {
					completedIndex = j;
				} else {
					break;
				}
			}
			minPoints = utils.stripClosest(points.subList(completedIndex, i + 1), minPoints);

		}

		return minPoints;
	}

	public MinimalDistancePoints call() throws IllegalArgumentException {
		return findClosestPoint();
	}
}
