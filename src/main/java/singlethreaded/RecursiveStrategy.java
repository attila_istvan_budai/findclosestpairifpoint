package singlethreaded;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import common.ClosestPointFinder;
import common.ClosestPointUtils;
import common.MinimalDistancePoints;
import common.Point;

public class RecursiveStrategy implements ClosestPointFinder {

	ClosestPointUtils utils;

	public RecursiveStrategy(ClosestPointUtils utils) {
		this.utils = utils;
	}

	public MinimalDistancePoints findClosestPoint(List<Point> points) throws IllegalArgumentException {
		Collections.sort(points, new Point.XComparator());
		return findClosestPairofPoints(points);
	}

	private MinimalDistancePoints findClosestPairofPoints(List<Point> points) throws IllegalArgumentException {
		int size = points.size();
		if (points.size() <= 3) {
			return utils.bruteForce(points);
		}
		int mid = size / 2;
		Point midPoint = points.get(mid);

		MinimalDistancePoints leftResult = findClosestPairofPoints(points.subList(0, mid));
		MinimalDistancePoints rightResult = findClosestPairofPoints(points.subList(mid, size));

		MinimalDistancePoints minResult = utils.getMinimalResult(rightResult, leftResult);

		List<Point> strip = new ArrayList<Point>();
		for (int i = 0; i < points.size(); i++) {
			if (utils.isAnyCoordinateDistanceIsLessThanMin(points.get(i), midPoint, minResult.getMin())) {
				strip.add(points.get(i));
			}
		}
		return utils.getMinimalResult(minResult, utils.stripClosest(strip, minResult));
	}

}
