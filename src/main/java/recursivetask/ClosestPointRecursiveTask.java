package recursivetask;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.RecursiveTask;

import common.ClosestPointUtils;
import common.MinimalDistancePoints;
import common.Point;

@SuppressWarnings("serial")
public class ClosestPointRecursiveTask extends RecursiveTask<MinimalDistancePoints> {

	List<Point> points;
	ClosestPointUtils utils;

	public ClosestPointRecursiveTask(List<Point> points, ClosestPointUtils utils) {
		this.points = points;
		this.utils = utils;
	}

	@Override
	protected MinimalDistancePoints compute() {
		int size = points.size();
		if (points.size() <= 3) {
			try {
				return utils.bruteForce(points);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		int mid = size / 2;
		Point midPoint = points.get(mid);

		ClosestPointRecursiveTask left = new ClosestPointRecursiveTask(points.subList(0, mid), utils);
		left.fork();
		ClosestPointRecursiveTask right = new ClosestPointRecursiveTask(points.subList(mid, size), utils);

		MinimalDistancePoints rightResult = right.compute();
		MinimalDistancePoints leftResult = left.join();
		MinimalDistancePoints minResult = utils.getMinimalResult(rightResult, leftResult);

		List<Point> strip = new ArrayList<Point>();
		for (int i = 0; i < points.size(); i++) {
			if (utils.isAnyCoordinateDistanceIsLessThanMin(points.get(i), midPoint, minResult.getMin())) {
				strip.add(points.get(i));
			}
		}
		return utils.getMinimalResult(minResult, utils.stripClosest(strip, minResult));

	}

}
