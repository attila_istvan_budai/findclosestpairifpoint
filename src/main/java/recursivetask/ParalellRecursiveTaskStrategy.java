package recursivetask;

import java.util.Collections;
import java.util.List;
import java.util.concurrent.ForkJoinPool;

import common.ClosestPointFinder;
import common.ClosestPointUtils;
import common.MinimalDistancePoints;
import common.Point;

public class ParalellRecursiveTaskStrategy implements ClosestPointFinder {

	private ClosestPointUtils utils;

	public ParalellRecursiveTaskStrategy(ClosestPointUtils utils) {
		this.utils = utils;
	}

	public MinimalDistancePoints findClosestPoint(List<Point> points) {
		Collections.sort(points, new Point.OtherCoordComparator(0));
		ForkJoinPool pool = new ForkJoinPool(Runtime.getRuntime().availableProcessors());
		ClosestPointRecursiveTask fbn = new ClosestPointRecursiveTask(points, utils);
		MinimalDistancePoints result = pool.invoke(fbn);
		return result;
	}
}
