package common;

public interface Distance {

	public Double distance(Point point1, Point point2);
}
