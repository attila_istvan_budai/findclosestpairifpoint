package common;

import java.util.Comparator;

public class Point {
	private Double[] coordinates;
	int lineNumber;

	public Point(Double[] coordinates) {
		this.coordinates = coordinates;
	}

	public Double[] getCoordinates() {
		return coordinates;
	}

	public void setCoordinates(Double[] coordinates) {
		this.coordinates = coordinates;
	}

	public int getLineNumber() {
		return lineNumber;
	}

	public void setLineNumber(int lineNumber) {
		this.lineNumber = lineNumber;
	}

	public static class XComparator implements Comparator<Point> {

		public int compare(Point o1, Point o2) {
			return (int) (o1.coordinates[0] - o2.coordinates[0]);
		}

	}

	public static class OtherCoordComparator implements Comparator<Point> {

		int index;

		public OtherCoordComparator(int index) {
			this.index = index;
		}

		public int compare(Point o1, Point o2) {
			return o1.coordinates[index].compareTo(o2.coordinates[index]);
		}

	}

	public String toString() {
		StringBuffer strBuff = new StringBuffer();
		strBuff.append(lineNumber + ":");
		for (int i = 0; i < this.coordinates.length; i++) {
			strBuff.append(coordinates[i] + " ");
		}
		return strBuff.toString();
	}

}
