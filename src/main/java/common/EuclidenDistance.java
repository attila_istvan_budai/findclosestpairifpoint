package common;

public class EuclidenDistance implements Distance {

	public Double distance(Point point1, Point point2) {
		Double[] coordinates1 = point1.getCoordinates();
		Double[] coordinates2 = point2.getCoordinates();
		Double sum = 0d;
		for (int i = 0; i < coordinates1.length; i++) {
			Double diff = (coordinates1[i] - coordinates2[i]) * (coordinates1[i] - coordinates2[i]);
			sum += diff;
		}
		return Math.sqrt(sum);
	}

}
