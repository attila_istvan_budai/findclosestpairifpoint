package common;

import java.util.List;

public interface ClosestPointFinder {

	public MinimalDistancePoints findClosestPoint(List<Point> points);

}
