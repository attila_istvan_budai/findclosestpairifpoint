package common;

import java.io.File;
import java.util.List;

import sweep.SweepPlaneStrategy;

/**
 * Hello world!
 *
 */
public class App {
	public static void main(String[] args) throws Exception {
		String filename = args[0];
		List<Point> points = ReadPointsFromFile.readFile(new File(filename));
		ClosestPointFinder finder = new SweepPlaneStrategy(new ClosestPointUtils(new EuclidenDistance()));
		System.out.println(finder.findClosestPoint(points).getResult());
	}
}
