package common;

public class MinimalDistancePoints {

	Point point1;
	Point point2;
	Double min;

	public MinimalDistancePoints(Point point1, Point point2, Double min) {
		this.point1 = point1;
		this.point2 = point2;
		this.min = min;
	}

	public Point getPoint1() {
		return point1;
	}

	public Point getPoint2() {
		return point2;
	}

	public Double getMin() {
		return min;
	}

	public int getMinLine() {
		if (point1.getLineNumber() < point2.getLineNumber()) {
			return point1.getLineNumber();
		} else {
			return point2.getLineNumber();
		}
	}

	public String getResult() {
		if (point1 != null && point2 != null) {
			if (point1.getLineNumber() < point2.getLineNumber()) {
				return point1.toString() + System.getProperty("line.separator") + point2.toString();
			} else {
				return point2.toString() + System.getProperty("line.separator") + point1.toString();
			}
		}
		return "some of the result is null";
	}
}
