package common;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class ReadPointsFromFile {

	public static List<Point> readFile(File fin) throws IOException {
		List<Point> points = new ArrayList<Point>();
		BufferedReader br = new BufferedReader(new FileReader(fin));
		int size = 0;

		String line = null;
		int lineNumber = 0;
		while ((line = br.readLine()) != null) {
			lineNumber++;
			String[] numberString = line.split("\t");
			size = numberString.length;
			Double[] coordinates = new Double[size];
			for (int i = 0; i < size; i++) {
				coordinates[i] = Double.parseDouble(numberString[i]);
			}
			Point point = new Point(coordinates); 
			point.setLineNumber(lineNumber);
			points.add(point);

		}

		br.close();
		return points;
	}
}
