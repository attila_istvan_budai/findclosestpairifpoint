package common;

import java.util.List;

public class ClosestPointUtils {

	private final Distance distanceCalculator;

	public ClosestPointUtils(Distance distance) {
		this.distanceCalculator = distance;
	}

	public MinimalDistancePoints getMinimalDistanceObject(Point point1, Point point2) {
		Double min = distanceCalculator.distance(point1, point2);
		return new MinimalDistancePoints(point1, point2, min);
	}

	public MinimalDistancePoints bruteForce(List<Point> points) throws IllegalArgumentException {
		if (points == null) {
			throw new IllegalArgumentException("List of points is null");
		}
		if (points.size() < 2) {
			throw new IllegalArgumentException("The size of points is less than 2");
		}
		MinimalDistancePoints result = getMinimalDistanceObject(points.get(0), points.get(1));
		return findMinimalDistance(points, result);
	}

	public MinimalDistancePoints getMinimalResult(MinimalDistancePoints result1, MinimalDistancePoints result2) {
		MinimalDistancePoints minResult;
		if (result1.getMin() < result2.getMin()) {
			minResult = result1;
		} else {
			minResult = result2;
		}
		return minResult;
	}

	public boolean isXoordinateDistanceIsLessThanMin(Point point1, Point point2, Double min) {
		if (Math.abs(point1.getCoordinates()[0] - point2.getCoordinates()[0]) < min) {
			return true;
		}
		return false;
	}

	public boolean isAnyCoordinateDistanceIsLessThanMin(Point point1, Point point2, Double min) {
		for (int i = 0; i < point1.getCoordinates().length; i++) {
			if (Math.abs(point1.getCoordinates()[i] - point2.getCoordinates()[i]) < min) {
				return true;
			}
		}
		return false;
	}

	public MinimalDistancePoints stripClosest(List<Point> points, MinimalDistancePoints minimalPoints) {
		return findMinimalDistance(points, minimalPoints);
	}

	private MinimalDistancePoints findMinimalDistance(List<Point> points, MinimalDistancePoints minimalPoints) {
		MinimalDistancePoints min = minimalPoints;
		for (int i = 0; i < points.size(); ++i) {
			for (int j = i + 1; j < points.size(); j++) {
				min = checkDistance(points.get(i), points.get(j), min);
			}
		}
		return min;
	}

	public MinimalDistancePoints findMinimalDistanceBetweenTwoList(List<Point> pointsLeft, List<Point> pointsRight, MinimalDistancePoints minimalPoints) {
		MinimalDistancePoints min = minimalPoints;
		if (pointsLeft.isEmpty() || pointsRight.isEmpty()) {
			return min;
		}
		for (int i = 0; i < pointsRight.size(); i++) {
			for (int j = 0; j < pointsLeft.size(); j++) {
				if (Math.abs(pointsRight.get(i).getCoordinates()[0] - pointsLeft.get(j).getCoordinates()[0]) < min.getMin()) {
					min = checkDistance(pointsLeft.get(j), pointsRight.get(i), min);
				}
			}
		}
		return min;
	}

	private MinimalDistancePoints checkDistance(Point pointsLeft, Point pointsRight, MinimalDistancePoints minimalPoints) {
		MinimalDistancePoints min = minimalPoints;
		double distance = distanceCalculator.distance(pointsRight, pointsLeft);
		if (distance < minimalPoints.getMin()) {
			min = new MinimalDistancePoints(pointsRight, pointsLeft, distance);
		}
		return min;
	}

	public MinimalDistancePoints minimalDistanceBetweenPointAndList(List<Point> pointList, Point point, MinimalDistancePoints minimalPoints) {
		MinimalDistancePoints min = minimalPoints;
		if (pointList.isEmpty()) {
			return min;
		}
		for (int i = 0; i < pointList.size(); i++) {
			min = checkDistance(pointList.get(i), point, min);
		}
		return min;
	}

}
