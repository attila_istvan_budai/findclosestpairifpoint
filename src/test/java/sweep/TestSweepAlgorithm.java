package sweep;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;

import common.ClosestPointFinder;
import common.ClosestPointUtils;
import common.EuclidenDistance;
import common.MinimalDistancePoints;
import common.Point;

public class TestSweepAlgorithm {

	@Test
	public void findClosestPointTestWith2Dimension() throws Exception {
		ClosestPointFinder sweep = new SweepPlaneStrategy(new ClosestPointUtils(new EuclidenDistance()));

		Point p = new Point(new Double[] { 0.0, 0.0 });
		Point p2 = new Point(new Double[] { 1.0, 1.0 });
		Point p3 = new Point(new Double[] { 2.0, 2.0 });
		Point p4 = new Point(new Double[] { 3.0, 2.5 });
		Point p5 = new Point(new Double[] { 4.0, 4.0 });
		Point p6 = new Point(new Double[] { 5.0, 5.0 });
		Point p7 = new Point(new Double[] { 6.0, 6.0 });
		Point p8 = new Point(new Double[] { 7.0, 9.0 });

		List<Point> points = new ArrayList<Point>();
		points.add(p);
		points.add(p2);
		points.add(p3);
		points.add(p4);
		points.add(p5);
		points.add(p6);
		points.add(p7);
		points.add(p8);
		MinimalDistancePoints result = sweep.findClosestPoint(points);

		Assert.assertEquals(Math.sqrt(1.25), result.getMin().doubleValue(), 0.001);

	}

	@Test
	public void findClosestPointTestWith2Dimension2() throws Exception {
		ClosestPointFinder sweep = new SweepPlaneStrategy(new ClosestPointUtils(new EuclidenDistance()));

		Point p = new Point(new Double[] { 0.0, 0.0 });
		Point p2 = new Point(new Double[] { 1.0, 1.0 });
		Point p3 = new Point(new Double[] { 3.0, 2.0 });
		Point p4 = new Point(new Double[] { 6.0, 2.5 });

		List<Point> points = new ArrayList<Point>();
		points.add(p);
		points.add(p2);
		points.add(p3);
		points.add(p4);
		MinimalDistancePoints result = sweep.findClosestPoint(points);

		Assert.assertEquals(Math.sqrt(2), result.getMin().doubleValue(), 0.001);

	}
}
