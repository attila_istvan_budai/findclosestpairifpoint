package recursivetask;

import java.util.ArrayList;
import java.util.List;

import junit.framework.Assert;

import org.junit.Before;
import org.junit.Test;

import common.ClosestPointFinder;
import common.ClosestPointUtils;
import common.EuclidenDistance;
import common.MinimalDistancePoints;
import common.Point;

public class ClosestPointRecursiveTaskTest {

	ClosestPointFinder finder;

	@Before
	public void setup() {
		finder = new ParalellRecursiveTaskStrategy(new ClosestPointUtils(new EuclidenDistance()));
	}

	@Test
	public void findClosestPointWithThreeElements() throws Exception {
		Point p = new Point(new Double[] { 0.0, 0.0, 0.0 });
		Point p2 = new Point(new Double[] { 1.0, 1.0, 1.0 });
		Point p3 = new Point(new Double[] { 2.0, 2.0, 2.0 });
		List<Point> points = new ArrayList<Point>();
		points.add(p);
		points.add(p2);
		points.add(p3);
		MinimalDistancePoints result = finder.findClosestPoint(points);
		Assert.assertEquals(Math.sqrt(3), result.getMin());
	}

	@Test
	public void findClosestPointTestWith2Dimension() throws Exception {
		Point p = new Point(new Double[] { 0.0, 0.0 });
		Point p2 = new Point(new Double[] { 0.0, 3.0 });
		Point p3 = new Point(new Double[] { 0.0, 5.0 });
		Point p4 = new Point(new Double[] { 0.0, 6.0 });
		Point p5 = new Point(new Double[] { 0.0, 8.0 });

		List<Point> points = new ArrayList<Point>();
		points.add(p);
		points.add(p2);
		points.add(p3);
		points.add(p4);
		points.add(p5);
		MinimalDistancePoints result = finder.findClosestPoint(points);
		Assert.assertEquals(1.0, result.getMin());

	}

	@Test
	public void findClosestPointTestWith2Dimension2() throws Exception {
		Point p = new Point(new Double[] { 0.0, 0.0 });
		Point p2 = new Point(new Double[] { 0.0, 3.0 });
		Point p3 = new Point(new Double[] { 0.0, 5.0 });
		Point p4 = new Point(new Double[] { 10.0, 6.0 });
		Point p5 = new Point(new Double[] { 10.0, 8.0 });

		List<Point> points = new ArrayList<Point>();
		points.add(p);
		points.add(p2);
		points.add(p3);
		points.add(p4);
		points.add(p5);
		MinimalDistancePoints result = finder.findClosestPoint(points);
		Assert.assertEquals(2.0, result.getMin());

	}
}