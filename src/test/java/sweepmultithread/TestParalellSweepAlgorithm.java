package sweepmultithread;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import common.ClosestPointFinder;
import common.ClosestPointUtils;
import common.EuclidenDistance;
import common.MinimalDistancePoints;
import common.Point;

public class TestParalellSweepAlgorithm {

	@Test
	public void findClosestPointTestWith2Dimension() throws Exception {
		ClosestPointFinder sweep = new ParalellSweepPlaneStrategy(new ClosestPointUtils(new EuclidenDistance()), 2);

		Point p = new Point(new Double[] { 0.0, 0.0 });
		Point p2 = new Point(new Double[] { 1.0, 3.0 });
		Point p3 = new Point(new Double[] { 2.0, 5.0 });
		Point p4 = new Point(new Double[] { 3.0, 6.0 });
		Point p5 = new Point(new Double[] { 4.0, 8.0 });
		Point p6 = new Point(new Double[] { 5.0, 8.0 });
		Point p7 = new Point(new Double[] { 6.0, 8.0 });
		Point p8 = new Point(new Double[] { 7.0, 8.0 });

		List<Point> points = new ArrayList<Point>();
		points.add(p);
		points.add(p2);
		points.add(p3);
		points.add(p4);
		points.add(p5);
		points.add(p6);
		points.add(p7);
		points.add(p8);
		MinimalDistancePoints result = sweep.findClosestPoint(points);

	}

	@Test
	public void separatorTest() throws Exception {
		ClosestPointFinder sweep = new ParalellSweepPlaneStrategy(new ClosestPointUtils(new EuclidenDistance()), 4);
		List<Point> points = new ArrayList<Point>();
		for (int i = 0; i < 23; i++) {
			points.add(new Point(new Double[] { (double) i, 8.0 }));
		}
		MinimalDistancePoints result = sweep.findClosestPoint(points);

	}

	@Test
	public void tooManyThreadSet() throws Exception {
		ClosestPointFinder sweep = new ParalellSweepPlaneStrategy(new ClosestPointUtils(new EuclidenDistance()), 4);
		Point p = new Point(new Double[] { 0.0, 0.0 });
		Point p2 = new Point(new Double[] { 1.0, 3.0 });
		List<Point> points = new ArrayList<Point>();
		points.add(p);
		points.add(p2);
		MinimalDistancePoints result = sweep.findClosestPoint(points);
	}
}
