package common;

import junit.framework.Assert;

import org.junit.Test;

public class MinimalDistancePointsTest {

	@Test
	public void getResultTest() {
		Point p1 = new Point(new Double[] { 3.0, 2.0 });
		Point p2 = new Point(new Double[] { 1.0, 3.0 });
		p1.setLineNumber(5);
		p2.setLineNumber(10);
		Double min = 1.0;

		MinimalDistancePoints minPoint = new MinimalDistancePoints(p1, p2, min);
		Assert.assertEquals(min, minPoint.getMin());
		Assert.assertEquals(p1.toString() + System.getProperty("line.separator") + p2.toString(), minPoint.getResult());

	}

	@Test
	public void getResultTest2() {
		Point p1 = new Point(new Double[] { 3.0, 2.0 });
		Point p2 = new Point(new Double[] { 1.0, 3.0 });
		p1.setLineNumber(10);
		p2.setLineNumber(5);
		Double min = 1.0;

		MinimalDistancePoints minPoint = new MinimalDistancePoints(p1, p2, min);

		Assert.assertEquals(min, minPoint.getMin());
		Assert.assertEquals(p2.toString() + System.getProperty("line.separator") + p1.toString(), minPoint.getResult());

	}

	@Test
	public void getMinLineInOrder() {
		Point p1 = new Point(new Double[] { 3.0, 2.0 });
		Point p2 = new Point(new Double[] { 1.0, 3.0 });
		p1.setLineNumber(1);
		p2.setLineNumber(2);
		MinimalDistancePoints minPoint = new MinimalDistancePoints(p1, p2, 1.0);

		Assert.assertEquals(1, minPoint.getMinLine());
	}

	@Test
	public void getMinLineReversedOrder() {
		Point p1 = new Point(new Double[] { 3.0, 2.0 });
		Point p2 = new Point(new Double[] { 1.0, 3.0 });
		p1.setLineNumber(2);
		p2.setLineNumber(1);
		MinimalDistancePoints minPoint = new MinimalDistancePoints(p1, p2, 1.0);

		Assert.assertEquals(1, minPoint.getMinLine());
	}

	@Test
	public void getResultTestWhenNullPointIsSet() {
		MinimalDistancePoints minPoint = new MinimalDistancePoints(null, null, 1.0);

		Assert.assertEquals("some of the result is null", minPoint.getResult());
	}

}
