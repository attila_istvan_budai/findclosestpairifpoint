package common;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import junit.framework.Assert;

import org.junit.Test;

public class PointTest {
	@Test
	public void pointToString() {
		Point p1 = new Point(new Double[] { 0.0, 0.0 });
		p1.setLineNumber(1);

		Assert.assertEquals("1:0.0 0.0 ", p1.toString());
	}

	@Test
	public void sortByXCoordinateTest() {
		Point p1 = new Point(new Double[] { 3.0, 0.0 });
		Point p2 = new Point(new Double[] { 1.0, 3.0 });
		Point p3 = new Point(new Double[] { 0.0, 0.0 });
		Point p4 = new Point(new Double[] { 2.0, 3.0 });

		List<Point> points = new ArrayList<Point>();
		points.add(p1);
		points.add(p2);
		points.add(p3);
		points.add(p4);

		Collections.sort(points, new Point.XComparator());

		Assert.assertEquals(p3, points.get(0));
		Assert.assertEquals(p2, points.get(1));
		Assert.assertEquals(p4, points.get(2));
		Assert.assertEquals(p1, points.get(3));
	}

	@Test
	public void sortBySecondCoordinateTest() {
		Point p1 = new Point(new Double[] { 3.0, 2.0 });
		Point p2 = new Point(new Double[] { 1.0, 3.0 });
		Point p3 = new Point(new Double[] { 0.0, 1.0 });
		Point p4 = new Point(new Double[] { 2.0, 0.0 });

		List<Point> points = new ArrayList<Point>();
		points.add(p1);
		points.add(p2);
		points.add(p3);
		points.add(p4);

		Collections.sort(points, new Point.OtherCoordComparator(1));

		Assert.assertEquals(p4, points.get(0));
		Assert.assertEquals(p3, points.get(1));
		Assert.assertEquals(p1, points.get(2));
		Assert.assertEquals(p2, points.get(3));
	}
}
