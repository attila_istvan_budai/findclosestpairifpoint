package common;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import junit.framework.Assert;

import org.junit.Before;
import org.junit.Test;

public class ClosestPointUtilsTest {

	private ClosestPointUtils utils;

	@Before
	public void setUp() {
		utils = new ClosestPointUtils(new EuclidenDistance());
	}

	@Test(expected = IllegalArgumentException.class)
	public void bruteForceTestWithNullList() throws IllegalArgumentException {
		utils.bruteForce(null);
	}

	@Test(expected = IllegalArgumentException.class)
	public void bruteForceTestWithZeroElement() throws IllegalArgumentException {
		List<Point> points = new ArrayList<Point>();
		utils.bruteForce(points);
	}

	@Test(expected = Exception.class)
	public void bruteForceTestWithOneElement() throws IllegalArgumentException {
		Point point = new Point(new Double[] { 1.0, 2.0 });

		List<Point> points = new ArrayList<Point>();
		points.add(point);
		utils.bruteForce(points);
	}

	@Test
	public void bruteForceTestWithSameElement() throws IllegalArgumentException {
		Point point = new Point(new Double[] { 1.0, 2.0 });

		List<Point> points = new ArrayList<Point>();
		points.add(point);
		points.add(point);
		MinimalDistancePoints diff = utils.bruteForce(points);
		Assert.assertEquals(0.0, diff.getMin());

	}

	@Test
	public void bruteForceTestWithTwoElements() throws IllegalArgumentException {
		Point point = new Point(new Double[] { 0.0, 0.0 });
		Point point2 = new Point(new Double[] { 0.0, 2.0 });

		List<Point> points = new ArrayList<Point>();
		points.add(point);
		points.add(point2);
		MinimalDistancePoints diff = utils.bruteForce(points);
		Assert.assertEquals(2.0, diff.getMin());

	}

	@Test
	public void bruteForceTestWithThreeElements() throws IllegalArgumentException {
		Point point = new Point(new Double[] { 0.0, 0.0 });
		Point point2 = new Point(new Double[] { 0.0, 2.0 });
		Point point3 = new Point(new Double[] { 0.0, 5.0 });

		List<Point> points = new ArrayList<Point>();
		points.add(point);
		points.add(point2);
		points.add(point3);
		MinimalDistancePoints diff = utils.bruteForce(points);
		Assert.assertEquals(2.0, diff.getMin());

	}

	@Test
	public void bruteForceTestWithThreeElements2() throws IllegalArgumentException {
		Point point = new Point(new Double[] { 0.0, 0.0 });
		Point point2 = new Point(new Double[] { 0.0, 3.0 });
		Point point3 = new Point(new Double[] { 0.0, 5.0 });

		List<Point> points = new ArrayList<Point>();
		points.add(point);
		points.add(point2);
		points.add(point3);
		MinimalDistancePoints diff = utils.bruteForce(points);
		Assert.assertEquals(2.0, diff.getMin());

	}

	@Test
	public void bruteForceTestWith3DimensionTwoElements() throws IllegalArgumentException {
		Point point = new Point(new Double[] { 0.0, 0.0, 0.0 });
		Point point2 = new Point(new Double[] { 0.0, 0.0, 3.0 });
		List<Point> points = new ArrayList<Point>();
		points.add(point);
		points.add(point2);
		MinimalDistancePoints diff = utils.bruteForce(points);
		Assert.assertEquals(3.0, diff.getMin());

	}

	@Test
	public void bruteForceTestWith3DimensionThreeElements() throws IllegalArgumentException {
		Point point = new Point(new Double[] { 0.0, 0.0, 0.0 });
		Point point2 = new Point(new Double[] { 0.0, 0.0, 2.0 });
		Point point3 = new Point(new Double[] { 0.0, 0.0, 3.0 });
		List<Point> points = new ArrayList<Point>();
		points.add(point);
		points.add(point2);
		points.add(point3);
		MinimalDistancePoints diff = utils.bruteForce(points);
		Assert.assertEquals(1.0, diff.getMin());

	}

	@Test
	public void bruteForceTestWith3DimensionThreeElements2() throws IllegalArgumentException {
		Point point = new Point(new Double[] { 0.0, 0.0, 0.0 });
		Point point2 = new Point(new Double[] { 1.0, 1.0, 1.0 });
		Point point3 = new Point(new Double[] { 2.0, 2.0, 2.0 });
		List<Point> points = new ArrayList<Point>();
		points.add(point);
		points.add(point2);
		points.add(point3);
		MinimalDistancePoints diff = utils.bruteForce(points);
		Assert.assertEquals(Math.sqrt(3), diff.getMin());

	}

	@Test
	public void isAnyCoordinateLessThanMinShouldReturnTrue() throws IllegalArgumentException {
		Point point1 = new Point(new Double[] { 0.0, 0.0 });
		Point point2 = new Point(new Double[] { 0.0, 1.0 });
		boolean isLess = utils.isAnyCoordinateDistanceIsLessThanMin(point1, point2, 2.0);
		Assert.assertEquals(true, isLess);
	}

	@Test
	public void isAnyCoordinateLessThanMinShouldReturnFalse() throws IllegalArgumentException {
		Point point1 = new Point(new Double[] { 0.0, 0.0 });
		Point point2 = new Point(new Double[] { 2.5, 3.0 });
		boolean isLess = utils.isAnyCoordinateDistanceIsLessThanMin(point1, point2, 2.0);
		Assert.assertEquals(false, isLess);
	}

	@Test
	public void getMinimalResultTest() {
		Point point1 = new Point(new Double[] { 0.0, 0.0 });
		Point point2 = new Point(new Double[] { 0.0, 1.0 });
		MinimalDistancePoints minDistance1 = new MinimalDistancePoints(point1, point2, 1.0);

		Point point3 = new Point(new Double[] { 0.0, 0.0 });
		Point point4 = new Point(new Double[] { 0.0, 2.0 });
		MinimalDistancePoints minDistance2 = new MinimalDistancePoints(point3, point4, 2.0);

		Assert.assertEquals(minDistance1, utils.getMinimalResult(minDistance1, minDistance2));
	}

	@Test
	public void getMinimalResultTest2() {
		Point point1 = new Point(new Double[] { 0.0, 0.0 });
		Point point2 = new Point(new Double[] { 0.0, 1.0 });
		MinimalDistancePoints minDistance1 = new MinimalDistancePoints(point1, point2, 1.0);

		Point point3 = new Point(new Double[] { 0.0, 0.0 });
		Point point4 = new Point(new Double[] { 0.0, 2.0 });
		MinimalDistancePoints minDistance2 = new MinimalDistancePoints(point3, point4, 2.0);

		Assert.assertEquals(minDistance1, utils.getMinimalResult(minDistance2, minDistance1));
	}

	@Test
	public void stripClosestTestShouldReturnTheMin() {
		Point point1 = new Point(new Double[] { 0.0, 0.0 });
		Point point2 = new Point(new Double[] { 0.0, 1.0 });
		MinimalDistancePoints minDistance = new MinimalDistancePoints(point1, point2, 1.0);

		Point point3 = new Point(new Double[] { 0.0, 3.0 });
		Point point4 = new Point(new Double[] { 1.0, 3.0 });
		Point point5 = new Point(new Double[] { 2.0, 3.0 });

		List<Point> points = new ArrayList<Point>();
		points.add(point3);
		points.add(point4);
		points.add(point5);

		Assert.assertEquals(minDistance, utils.stripClosest(points, minDistance));
	}

	@Test
	public void stripClosestTestShouldReturnTheP3() {
		Point point1 = new Point(new Double[] { 0.0, 0.0 });
		Point point2 = new Point(new Double[] { 0.0, 3.0 });
		MinimalDistancePoints minDistance = new MinimalDistancePoints(point1, point2, 3.0);

		Point point3 = new Point(new Double[] { 0.0, 0.5 });
		Point point4 = new Point(new Double[] { 0.0, 3.0 });
		Point point5 = new Point(new Double[] { 6.0, 3.0 });

		List<Point> points = new ArrayList<Point>();
		points.add(point3);
		points.add(point4);
		points.add(point5);

		MinimalDistancePoints result = utils.stripClosest(points, minDistance);
		Assert.assertEquals(2.5, result.getMin());
		Assert.assertEquals(point3, result.getPoint2());
		Assert.assertEquals(point4, result.getPoint1());
	}

	@Test
	public void isXoordinateDistanceIsLessThanMinTestShouldReturnTrue() {
		Point point1 = new Point(new Double[] { 0.0, 0.0 });
		Point point2 = new Point(new Double[] { 1.0, 0.0 });
		boolean isTrue = utils.isXoordinateDistanceIsLessThanMin(point1, point2, 2.0);
		Assert.assertTrue(isTrue);
	}

	@Test
	public void isXoordinateDistanceIsLessThanMinTestShouldReturnFalse() {
		Point point1 = new Point(new Double[] { 0.0, 0.0 });
		Point point2 = new Point(new Double[] { 1.0, 0.0 });
		boolean isFalse = utils.isXoordinateDistanceIsLessThanMin(point1, point2, 0.5);
		Assert.assertFalse(isFalse);
	}

	@Test
	public void findMinimalDistanceBetweenTwoWithEmptyListShouldReturnMin() {
		Point point1 = new Point(new Double[] { 0.0, 0.0 });
		Point point2 = new Point(new Double[] { 0.0, 3.0 });
		MinimalDistancePoints minDistance = new MinimalDistancePoints(point1, point2, 3.0);
		MinimalDistancePoints res = utils.findMinimalDistanceBetweenTwoList(Collections.<Point> emptyList(), Collections.<Point> emptyList(), minDistance);
		Assert.assertEquals(minDistance, res);

	}

	@Test
	public void findMinimalDistanceBetweenTwoWithTwoListsShouldReturnSameMin() {
		Point point1 = new Point(new Double[] { -1.0, 0.0 });
		Point point2 = new Point(new Double[] { -2.0, 3.0 });

		Point point3 = new Point(new Double[] { 1.0, 0.0 });
		Point point4 = new Point(new Double[] { 2.0, 3.0 });

		List<Point> pointsLeft = new ArrayList<Point>();
		pointsLeft.add(point1);
		pointsLeft.add(point2);
		List<Point> pointsRight = new ArrayList<Point>();
		pointsRight.add(point3);
		pointsRight.add(point4);
		MinimalDistancePoints minDistance = new MinimalDistancePoints(point1, point2, 1.0);
		MinimalDistancePoints res = utils.findMinimalDistanceBetweenTwoList(pointsLeft, pointsRight, minDistance);
		Assert.assertEquals(minDistance, res);

	}

	@Test
	public void findMinimalDistanceBetweenTwoWithTwoListsShouldReturnDifferentMin() {
		Point point1 = new Point(new Double[] { -1.0, 0.0 });
		Point point2 = new Point(new Double[] { -2.0, 3.0 });

		Point point3 = new Point(new Double[] { 1.0, 0.0 });
		Point point4 = new Point(new Double[] { 2.0, 3.0 });

		List<Point> pointsLeft = new ArrayList<Point>();
		pointsLeft.add(point1);
		pointsLeft.add(point2);
		List<Point> pointsRight = new ArrayList<Point>();
		pointsRight.add(point3);
		pointsRight.add(point4);
		MinimalDistancePoints minDistance = new MinimalDistancePoints(point1, point2, 3.0);
		MinimalDistancePoints res = utils.findMinimalDistanceBetweenTwoList(pointsLeft, pointsRight, minDistance);
		Assert.assertEquals(2.0, res.getMin());

	}

	@Test
	public void minimalDistanceBetweenPointAndListEmptyListShouldReturnMin() {
		Point point1 = new Point(new Double[] { 0.0, 0.0 });
		Point point2 = new Point(new Double[] { 0.0, 3.0 });
		MinimalDistancePoints minDistance = new MinimalDistancePoints(point1, point2, 3.0);
		MinimalDistancePoints res = utils.minimalDistanceBetweenPointAndList(Collections.<Point> emptyList(), point1, minDistance);
		Assert.assertEquals(minDistance, res);

	}

	@Test
	public void minimalDistanceBetweenPointAndListShouldReturnMin() {
		Point point1 = new Point(new Double[] { 0.0, 0.0 });
		Point point2 = new Point(new Double[] { 0.0, 3.0 });

		Point point3 = new Point(new Double[] { 5.0, 0.0 });
		Point point4 = new Point(new Double[] { 6.0, 3.0 });
		List<Point> points = new ArrayList<Point>();
		points.add(point3);
		points.add(point4);
		MinimalDistancePoints minDistance = new MinimalDistancePoints(point1, point2, 2.0);
		MinimalDistancePoints res = utils.minimalDistanceBetweenPointAndList(points, point1, minDistance);
		Assert.assertEquals(minDistance, res);

	}

	@Test
	public void minimalDistanceBetweenPointAndListShouldReturnDifferentMin() {
		Point point1 = new Point(new Double[] { 0.0, 3.0 });
		Point point2 = new Point(new Double[] { 0.0, 3.0 });

		Point point3 = new Point(new Double[] { 3.0, 3.0 });
		Point point4 = new Point(new Double[] { 6.0, 3.0 });
		List<Point> points = new ArrayList<Point>();
		points.add(point3);
		points.add(point4);
		MinimalDistancePoints minDistance = new MinimalDistancePoints(point1, point2, 6.0);
		MinimalDistancePoints res = utils.minimalDistanceBetweenPointAndList(points, point1, minDistance);
		Assert.assertEquals(3.0, res.min);

	}

}
