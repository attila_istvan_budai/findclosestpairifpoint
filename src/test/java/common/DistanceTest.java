package common;

import junit.framework.Assert;

import org.junit.Test;

public class DistanceTest {

	@Test
	public void distanceTest() {
		Distance distance = new EuclidenDistance();
		Point p1 = new Point(new Double[] { 0d, 0d });
		Point p2 = new Point(new Double[] { 1d, 1d });

		Double d = distance.distance(p1, p2);
		Assert.assertEquals(Math.sqrt(2), d);
	}

	@Test
	public void distanceTest2() {
		Distance distance = new EuclidenDistance();
		Point p1 = new Point(new Double[] { 0.5306453824534234, 0.7966312392693505 });
		Point p2 = new Point(new Double[] { 0.5306453230960056, 0.7966312271078694 });

		Double d = distance.distance(p1, p2);
		System.out.println(d);

		Point p3 = new Point(new Double[] { 0.03663538416309109, 0.08697520539359826 });
		Point p4 = new Point(new Double[] { 0.03663532480567322, 0.08697517833095603 });

		Double d2 = distance.distance(p3, p4);
		System.out.println(d);
	}

}
