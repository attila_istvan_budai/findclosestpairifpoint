package functionaltests;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Random;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import recursivetask.ParalellRecursiveTaskStrategy;
import singlethreaded.RecursiveStrategy;
import sweep.SweepPlaneStrategy;
import sweepmultithread.ParalellSweepPlaneStrategy;

import common.ClosestPointFinder;
import common.ClosestPointUtils;
import common.EuclidenDistance;
import common.MinimalDistancePoints;
import common.Point;

@RunWith(Parameterized.class)
public class PerformanceTest {

	int size;

	@Parameters
	public static Collection<Object[]> data() {
		return Arrays.asList(new Object[][] { { 1000 }, { 10000 }, { 100000 }, { 1000000 }, { 1000000 }, { 10000000 } });
	}

	Random rand = new Random();

	public Point generateRandomPoint() {
		return new Point(new Double[] { rand.nextDouble() * 100, rand.nextDouble() * 100 });
	}

	public PerformanceTest(int size) {
		this.size = size;
	}

	@Test
	public void test() throws Exception {

		System.out.println("=====================================");
		System.out.println("Test with " + size + " numbers");
		System.out.println("=====================================");
		List<Point> points = new ArrayList<Point>();
		for (int i = 0; i < size; i++) {
			points.add(generateRandomPoint());
		}

		long pt = System.currentTimeMillis();
		ClosestPointFinder caller = new ParalellRecursiveTaskStrategy(new ClosestPointUtils(new EuclidenDistance()));
		MinimalDistancePoints result = caller.findClosestPoint(points);
		System.out.println(result.getResult());
		System.out.println("Total time of recursive algorithm " + (System.currentTimeMillis() - pt));
		System.out.println("-----------------------------------------");

		pt = System.currentTimeMillis();
		ClosestPointFinder finder = new RecursiveStrategy(new ClosestPointUtils(new EuclidenDistance()));
		MinimalDistancePoints result2 = finder.findClosestPoint(points);

		System.out.println(result2.getResult());
		System.out.println("Total time of sequential algorithm: " + (System.currentTimeMillis() - pt));
		System.out.println("=====================================");
		System.out.println();

		pt = System.currentTimeMillis();
		ClosestPointFinder sweep = new SweepPlaneStrategy(new ClosestPointUtils(new EuclidenDistance()));
		MinimalDistancePoints result3 = sweep.findClosestPoint(points);
		System.out.println(result3.getResult());
		System.out.println("Total time of sweep algorithm: " + (System.currentTimeMillis() - pt));
		System.out.println("=====================================");

		pt = System.currentTimeMillis();
		ClosestPointFinder paralellSweep = new ParalellSweepPlaneStrategy(new ClosestPointUtils(new EuclidenDistance()), 4);
		MinimalDistancePoints result4 = paralellSweep.findClosestPoint(points);
		System.out.println(result4.getResult());
		System.out.println("Total time of sweep algorithm: " + (System.currentTimeMillis() - pt));
		System.out.println("=====================================");
		System.out.println("Results: ");

		System.out.println(result.getMin());
		System.out.println(result2.getMin());
		System.out.println(result3.getMin());
		System.out.println(result4.getMin());
		System.out.println("=====================================");

	}
}
