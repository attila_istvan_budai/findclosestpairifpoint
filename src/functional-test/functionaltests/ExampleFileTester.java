package functionaltests;

import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;
import java.nio.file.Files;
import java.util.List;

import junit.framework.Assert;

import org.junit.Test;

import recursivetask.ParalellRecursiveTaskStrategy;
import singlethreaded.RecursiveStrategy;
import sweep.SweepPlaneStrategy;
import sweepmultithread.ParalellSweepPlaneStrategy;

import common.ClosestPointFinder;
import common.ClosestPointUtils;
import common.EuclidenDistance;
import common.MinimalDistancePoints;
import common.Point;
import common.ReadPointsFromFile;

public class ExampleFileTester {

	public static File[] filterForTsv(String dirName) {
		File dir = new File(dirName);
		return dir.listFiles(new FilenameFilter() {
			public boolean accept(File dir, String filename) {
				return filename.endsWith(".tsv");
			}
		});

	}

	private String getResultForFile(File file) throws IOException {
		File f = new File(file.getAbsolutePath().replace(".tsv", ".txt").replace("input", "output"));
		return new String(Files.readAllBytes(f.toPath()));
	}

	@Test
	public void test() throws Exception {
		File[] files = filterForTsv(".\\sample_in_out\\");
		for (File file : files) {
			System.out.println(file.toString());
			List<Point> points = ReadPointsFromFile.readFile(file);
			long pt = System.currentTimeMillis();
			ClosestPointFinder finder = new RecursiveStrategy(new ClosestPointUtils(new EuclidenDistance()));
			MinimalDistancePoints result2 = finder.findClosestPoint(points);

			System.out.println(getResultForFile(file));
			System.out.println(result2.getResult());
			System.out.println("Total time: " + (System.currentTimeMillis() - pt));
			System.out.println();
			Assert.assertEquals(Integer.parseInt(getResultForFile(file).split(":")[0]), result2.getMinLine());
		}
	}

	@Test
	public void test2() throws IOException {
		File[] files = filterForTsv(".\\sample_in_out\\");
		for (File file : files) {
			System.out.println(file.toString());
			List<Point> points = ReadPointsFromFile.readFile(file);
			long pt = System.currentTimeMillis();
			ClosestPointFinder caller = new ParalellRecursiveTaskStrategy(new ClosestPointUtils(new EuclidenDistance()));
			MinimalDistancePoints result = caller.findClosestPoint(points);
			System.out.println(" -- Total time: " + (System.currentTimeMillis() - pt));
			System.out.println(result.getResult());
			System.out.println(getResultForFile(file));
			System.out.println();
			Assert.assertEquals(Integer.parseInt(getResultForFile(file).split(":")[0]), result.getMinLine());
		}
	}

	@Test
	public void sweepTest() throws Exception {
		File[] files = filterForTsv(".\\sample_in_out\\");
		for (File file : files) {
			System.out.println(file.toString());
			List<Point> points = ReadPointsFromFile.readFile(file);
			long pt = System.currentTimeMillis();
			ClosestPointFinder sweep = new SweepPlaneStrategy(new ClosestPointUtils(new EuclidenDistance()));
			MinimalDistancePoints result = sweep.findClosestPoint(points);
			System.out.println(" -- Total time: " + (System.currentTimeMillis() - pt));
			System.out.println(result.getResult());
			System.out.println(getResultForFile(file));
			System.out.println();
			Assert.assertEquals(Integer.parseInt(getResultForFile(file).split(":")[0]), result.getMinLine());
		}
	}

	@Test
	public void paralellSweepTest() throws Exception {
		File[] files = filterForTsv(".\\sample_in_out\\");
		for (File file : files) {
			System.out.println(file.toString());
			List<Point> points = ReadPointsFromFile.readFile(file);
			long pt = System.currentTimeMillis();
			ClosestPointFinder sweep = new ParalellSweepPlaneStrategy(new ClosestPointUtils(new EuclidenDistance()), 2);
			MinimalDistancePoints result = sweep.findClosestPoint(points);
			System.out.println(" -- Total time: " + (System.currentTimeMillis() - pt));
			System.out.println(result.getResult());
			System.out.println(getResultForFile(file));
			System.out.println();
			Assert.assertEquals(Integer.parseInt(getResultForFile(file).split(":")[0]), result.getMinLine());

		}
	}

}
